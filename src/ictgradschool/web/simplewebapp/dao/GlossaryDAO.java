package ictgradschool.web.simplewebapp.dao;
import ictgradschool.web.simplewebapp.glossary.GlossaryPOJO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class GlossaryDAO implements AutoCloseable {

    private final Connection conn;

    public GlossaryDAO() throws SQLException {
        this.conn = HikariConnectionPool.getConnection();
    }

    public List<GlossaryPOJO> getGlossaryEntries() throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM PGCert_glossary")) {
            try (ResultSet rs = stmt.executeQuery()) {
                List<GlossaryPOJO> glossaryEntries = new ArrayList<>();
                while (rs.next()) {
                    glossaryEntries.add(glossaryEntryFromResultSet(rs));
                }
                return glossaryEntries;
            }
        }
    }

    public GlossaryPOJO getGlossaryEntryById(int id) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM PGCert_glossary WHERE id = ?")) {
            stmt.setInt(1, id);
            try (ResultSet rs = stmt.executeQuery()) {
                if (rs.next()) {
                    return glossaryEntryFromResultSet(rs);
                } else {
                    return null;
                }
            }
        }
    }

    private GlossaryPOJO glossaryEntryFromResultSet(ResultSet rs) throws SQLException {
        return new GlossaryPOJO(rs.getInt(1), rs.getString(2), rs.getString(3));
    }

    @Override
    public void close() throws SQLException {
        this.conn.close();
    }

}
