package ictgradschool.web.simplewebapp.glossary;

public class GlossaryPOJO {

    private Integer id;
    private String word;
    private String definition;


    public GlossaryPOJO(Integer id, String word, String definition) {
        this.id = id;
        this.word = word;
        this.definition = definition;

    }

    public GlossaryPOJO(String fname, String lname, String bio) {
        this.word = fname;
        this.definition = lname;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public String getWord() {
        return word;
    }

    public String getDefinition() {
        return definition;
    }


}
