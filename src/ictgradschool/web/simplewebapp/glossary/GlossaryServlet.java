package ictgradschool.web.simplewebapp.glossary;

import ictgradschool.web.simplewebapp.dao.GlossaryDAO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class GlossaryServlet extends HttpServlet {
    List<GlossaryPOJO> words;
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //THIS IS JUST TEST CODE TO SEE IF EVERYTHING IS CONNECTING OK
        try {
            GlossaryDAO dao = new GlossaryDAO();
            GlossaryPOJO test;
            test = dao.getGlossaryEntryById(6);

            words = dao.getGlossaryEntries();
            System.out.println("Get by id '1' test: " + test.getWord() + " = " + test.getDefinition());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println("testing thiings");


        request.getSession().setAttribute("allOurData", words); //might delete
        //THIS IS JUST TEST CODE TO SEE IF EVERYTHING IS CONNECTING OK

        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/glossary/glossary.jsp");
        dispatcher.forward(request, response);

    }


}
