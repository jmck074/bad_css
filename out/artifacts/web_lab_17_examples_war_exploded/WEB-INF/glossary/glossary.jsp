<%--
  Created by IntelliJ IDEA.
  User: tcro142
  Date: 3/10/2018
  Time: 8:22 AM
  To change this template use File | Settings | File Templates.
--%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>

    <title>PGCert Glossary</title>
    <%--&lt;%&ndash;  &lt;%&ndash;note how the css href is setup&ndash;%&gt;--%>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/my-styles.css">
    <style src="css/my-styles.css"></style>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>


</head>
<body>
<img id="git" src="${pageContext.request.contextPath}/images/nrgc.gif">

<p class="para">
    The following is a glossary of key words and definitions used in the PGCert’ CS718 - Programming for Industry and
    CS719Programming with Web Technologies courses. Use this glossary to lookup key words and definitions from these
    courses.
</p>

<table>
    <c:forEach var="i" begin="0" end="18">
        <tr>
            <td><p>${allOurData[i].getWord()}</p></td>
            <td><p>${allOurData[i].getDefinition()}</p></td>
        </tr>
    </c:forEach>

</table>
consider displaying this information in a sidebar or similar
<h2>CS718</h2>
<p class="cs">An examination of object-oriented programming and design. Key principles of object-oriented programming:
    typing,
    encapsulation, inheritance, polymorphism and composition. Fundamental object-oriented modelling and design
    techniques. Students will develop application software of reasonable complexity that draws on object-oriented
    language features, and contemporary APIs, frameworks and tools.</p>


<h2>CS719</h2>
<p class="cs">A practical exploration of tools & techniques used to create modern, responsive websites and web apps. In
    this course
    we will learn how technologies such as HTML5, CSS, JavaScript, Servlets, and SQL fit together to create the websites
    you see and use every day.</p>

<h2>Main Content:</h2>
<p> The main content outlined within the task description; e.g.:</p>


<br>
This is how you link to an image that is outside of the web-inf folder from inside of the web-inf folder:
<img id="trex" src="${pageContext.request.contextPath}/images/t-rex-cry.png">


<footer>
    <div class="jumbotron bg-primary p-3">
        <div class="row">
            <div class="col-sm-5"> The PGCert is delivered by the Auckland ICT Graduate School</div>
        </div>
    </div>
</footer>
<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="navbar-brand" href="#">About Us</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">Home</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">Related Links</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">FAQs</a>
        </li>
    </ul>
</nav>
</body>
</html>
