drop table if exists PGCert_glossary;

CREATE TABLE IF NOT EXISTS PGCert_glossary (
  id integer not null primary key auto_increment,
  word VARCHAR(27) CHARACTER SET utf8,
  definition VARCHAR(206) CHARACTER SET utf8
);

INSERT INTO PGCert_glossary(word, definition) VALUES
  ('Arrays','An indexed data structure of a fixed length in which all objects have the same type.'),
  ('Object oriented programming','A programming language model organized around objects rather than "actions" and data rather than logic.'),
  ('Inheritance','A core concept in OO program that relates to the ability of new objects to take on the properties of existing ones. '),
  ('Exception handling','The process of responding to the occurrence, during computation, of anomalous or exceptional conditions requiring special processing – often changing the normal flow of program execution.'),
  ('Collections','A set of classes and interfaces that implement commonly reusable collection data structures. Although referred to as a framework, it works in a manner of a library.'),
  ('Swing','The GUI widget toolkit for Java. It is part of Oracle''s Java Foundation Classes (JFC) – an API for providing a graphical user interface (GUI) for Java.'),
  ('Concurrency','The ability of different parts or units of a program, algorithm, or problem to be executed out-of-order or in partial order, without affecting the final outcome. '),
  ('CSS','A style sheet language used for describing the presentation of a document written in a markup language like HTML.'),
  ('HTML','The standard markup language for creating Web pages that describes the structure of Web pages using markup'),
  ('JavaScript','The programming language of HTML and the web that is a high-level, interpreted programming language. '),
  ('SQL','A domain-specific language used in programming and designed for managing data held in a relational database management system (RDBMS).'),
  ('JDBC','An application programming interface (API) for the programming language Java, which defines how a client may access a database. It is a Java-based data access technology used for Java database connectivity.'),
  ('DAOs','An object or an interface that provides access to an underlying database or any other persistence storage.'),
  ('Servlets','A Java software component that extends the capabilities of a server. They most commonly implement web containers for hosting web applications on web servers.'),
  ('Cookies','A small piece of data sent from a website and stored on the user''s computer by the user''s web browser.'),
  ('Sessions','Allow the server to store key/value pairs on the server-side, with each client having its entries identified with a long, unique id.'),
  ('JSP','A technology that helps software developers create dynamically generated web pages based on HTML, XML, or other document types. '),
  ('JSTL','A collection of useful JSP tags which encapsulates the core functionality common to many JSP applications. ');
